const wheel_states = {
	STRAIGHT : 'straight',
	LEFT : 'left',
	RIGHT : 'right'
}

const accelerator_states = {
	OFF : 'off',
	ACCELERATE : 'accelerate',
	DECELERATE : 'decelerate'
}

// Object's values for random retrieval
let wheel_states_list = Object.values(wheel_states);
let accelerator_states_list = Object.values(accelerator_states);

// Returns a random wheelState
var randomWheelState = function() {
    return wheel_states_list[wheel_states_list.length * Math.random() << 0];
}

// Returns a random acceleratorState
var randomAcceleratorState = function() {
    return accelerator_states_list[accelerator_states_list.length * Math.random() << 0];
}

// Models a gene
// Gene is a tuple of steering wheel direction, accelerator state
class Gene
{
    // length: number of genes
    constructor(wheel_state, accelerator_state)
    {
	this.wheel_state = wheel_state;
	this.accelerator_state = accelerator_state;
    }
};
