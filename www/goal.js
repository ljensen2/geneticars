class Goal
{
    constructor(x, y, size)
    {
        this.size = size;
        this.pos = createVector(x, y);
    }

    show()
    {
	push();
	translate(this.pos.x, this.pos.y);
	image(goalImage, 0, 0, this.size, this.size);
	pop();
    }

    // Returns true if car collides with goal
    // and false otherwise
    collidesWith(car) {
	//TODO: Rethink?
	let carRectP1 = createVector(car.pos.x - (car.carSize / 2), car.pos.y - (car.carSize / 2));
	let carRectP2 = createVector(car.pos.x + (car.carSize / 2), car.pos.y + (car.carSize / 2));
	let goalRectP1 = createVector(this.pos.x, this.pos.y);
	let goalRectP2 = createVector(this.pos.x + this.size, this.pos.y + this.size);

	// If one rectangle is on left side of other 
	if (carRectP1.x >= goalRectP2.x || goalRectP1.x >= carRectP2.x) 
	    return false; 
      
	// If one rectangle is above other 
	if (carRectP2.y <= goalRectP1.y || goalRectP2.y <= carRectP1.y) 
	    return false; 
      
	return true; 
    }
}
