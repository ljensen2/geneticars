
class Obstacle
{
	
    // x: pos x
    // y: pos y
    // width: width of obstacle
    // height: height of obstacle
    constructor(x, y, width, height)
    {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
    }

    // Displays the obstacle (rectangle)
    show()
    {
        push();

        fill(0, 0, 50);
	translate(this.x, this.y);
	
        rect(0, 0, this.width, this.height);
        pop();
    }

    // Returns true if car collides with Obstacle, false if not
    collidesWith(car) {
	//TODO: Rethink?
	let carRectP1 = createVector(car.pos.x - (car.carSize / 2), car.pos.y - (car.carSize / 2));
	let carRectP2 = createVector(car.pos.x + (car.carSize / 2), car.pos.y + (car.carSize / 2));
	let obsRectP1 = createVector(this.x, this.y);
	let obsRectP2 = createVector(this.x + this.width, this.y + this.height);

	// If one rectangle is on left side of other 
	if (carRectP1.x >= obsRectP2.x || obsRectP1.x >= carRectP2.x) 
	    return false; 
      
	// If one rectangle is above other 
	if (carRectP2.y <= obsRectP1.y || obsRectP2.y <= carRectP1.y) 
	    return false; 

	return true; 
	
    }

    // Returns true if vector in obstacle
    collidesWithPoint(vector) {
	let obsRectP1 = createVector(this.x, this.y);
	let obsRectP2 = createVector(this.x + this.width, this.y + this.height);
	if(vector.x < obsRectP1.x || vector.x > obsRectP2.x) {
	    return false;
	}
	if(vector.y < obsRectP1.y || vector.y > obsRectP2.y) {
            return false;
	}
	return true;

    }
}
