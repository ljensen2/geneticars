let acceleratorStrength = 0.4;
let dragStrength = 0.02; // Constant drag
let turnStrength = 0.06;


class Car 
{
	
    // x: initial pos x
    // y: initial pos y
    // dnaLength: length of DNA (E.g. number of frames per generation)
    constructor(x, y, dnaLength, carSize, mutateChance)
    {
        this.pos = createVector(x, y);
	this.lastCheckmark = createVector(x, y);
	this.mutateChance = mutateChance;
	this.odometer = 0; // Distance
	this.carSize = carSize;
        this.carWidth = carSize / 2;
        this.carLength = carSize;
	this.acceleration = 0; // Only one dimension, relies on headingDir
	this.headingDir = createVector(); // Heading direction vector

        this.dna = new DNA(dnaLength, mutateChance);
        this.score = 0;
        this.step = 0;
	this.colorVal = Math.random() * 255;
	this.dead = false; // Set to true if collides with wall (or goal)
        this.foundGoal = false; // Set to true if collides with goal
	this.deadStep = 0; // Step car died on
    }

    update()
    {
	if(this.dead) { // Found goal or collided with wall
	    return; // TODO: implemenet
	}

	let calcDist = this.pos.dist(this.lastCheckmark);
	if(calcDist > 3 * this.carSize) {
            this.odometer += calcDist;
	    this.lastCheckmark = this.pos;
	}

	let currentGene = this.dna.dna[this.step++];

	switch(currentGene.wheel_state) { // Calculate headingDir
	    case wheel_states.STRAIGHT: // Keep steering direction
		break;
	    case wheel_states.LEFT: // Turn left defined amount
		this.headingDir = p5.Vector.fromAngle(this.headingDir.heading() + turnStrength);
		break;
	    case wheel_states.RIGHT: // Turn right defined amount
		this.headingDir = p5.Vector.fromAngle(this.headingDir.heading() - turnStrength);
		break;

	}

	switch(currentGene.accelerator_state) { // Calculate acceleration
	    case accelerator_states.OFF: // Do not keep accelerating
		break;
	    case accelerator_states.ACCELERATE: // Accelerate
		this.acceleration += acceleratorStrength;
		break;
	    case accelerator_states.DECELERATE: // Decelerate (weaker)
		this.acceleration -= acceleratorStrength / 2;
		break;
	}
	this.acceleration -= dragStrength;

	let accelVector = this.headingDir.copy();
	accelVector.setMag(this.acceleration);
	    
	this.pos.add(accelVector);
    }

    setDNA(newDNA)
    {
	// sets this dna to copy of newDNA
        this.dna.dna = newDNA.dna.slice();
    }

    setScore(score)
    {
	this.score = score;
    }

    // Displays the Car
    show()
    {
        push();

        fill(this.colorVal, 255, 255);
	translate(this.pos.x + (this.carLength / 2), this.pos.y + (this.carWidth / 2));
	rotate(this.headingDir.heading());
	
	// body
        rect(0 - (this.carLength / 2), 0 - (this.carWidth/ 2), this.carLength, this.carWidth);
	image(carImage, 0 - (this.carLength / 2), 0 - (this.carWidth / 2), this.carLength, this.carWidth);
        pop();
    }
}
